// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Home from './components/Home'
import router from './router'
import Header from './components/Header'
import Footer from './components/Footer'
import Hello from './components/HelloWorld'
import Content from './components/Content'
import ProductServices from './components/ProductServices'
import Steps from './components/Steps'
import FlexibleEdi from './components/FlexibleEdi'
import DownloadApp from './components/DownloadApp'
import MapPage from './components/MapPage'
import './plugins/bootstrap-vue'

Vue.component('headers', Header)
Vue.component('home', Home)
Vue.component('footers', Footer)
Vue.component('hello', Hello)
Vue.component('contents', Content)
Vue.component('ProductServices', ProductServices)
Vue.component('steps', Steps)
Vue.component('FlexibleEdi', FlexibleEdi)
Vue.component('DownloadApp', DownloadApp)
Vue.component('MapPage', MapPage)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
