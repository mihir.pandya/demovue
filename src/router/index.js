import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import POS from '@/components/POS'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/pos',
      name: 'POS',
      component: POS
    }
  ],
  mode: 'history'
})
